public class TriangleClass {

    public String triangleType(int side1, int side2, int side3) {

        if((side1 > 0 && side2 > 0 && side3 > 0) && ((side1 + side2 + side3) == 180)) {

            if (side1 == side2 && side2 == side3)
                return "Equilateral triangle";

            else if (side1 == side2 || side2 == side3 || side3 == side1)
                return "Isosceles triangle";

            else
                return "Scalene triangle";
        }
        return "Invalid triangle";
    }
}
