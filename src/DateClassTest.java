import org.junit.jupiter.api.Test;

import java.util.Date;
import static org.junit.jupiter.api.Assertions.*;

public class DateClassTest {

    private Date testDate = new Date(0);
    private DateClass dateClass = new DateClass(testDate);

    @Test
    void testDateGet() {
        String returnTime = dateClass.getDate();
        assertEquals("01/01/1970", returnTime);
    }

    @Test
    void testDateAdvancePos() {
        dateClass.advance(1);
        String returnTime = dateClass.getDate();
        assertEquals("02/01/1970", returnTime);
    }

    @Test
    void testDateAdvanceNeg() {
        dateClass.advance(-1);
        String returnTime = dateClass.getDate();
        assertEquals("31/12/1969", returnTime);
    }

    @Test
    void testDateYear() {
        dateClass.advance(365);
        String returnTime = dateClass.getDate();
        assertEquals("01/01/1971", returnTime);
    }

}
