import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateClass {

    private Date date;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    public DateClass(Date date) {
        this.date = date;
    }

    public String getDate() {
        return sdf.format(date);
    }

    public void advance(int day) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, day);
        date = cal.getTime();
    }
}





