import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TriangleClassTest {
    private TriangleClass triangleClass = new TriangleClass();

    @Test
    void testBoundary() {
        String returnString = triangleClass.triangleType(60,60,59);
        assertSame("Invalid triangle", returnString);

        returnString = triangleClass.triangleType(60,60,61);
        assertSame("Invalid triangle", returnString);
    }

    @Test
    void testZero() {
        String returnString = triangleClass.triangleType(-1, 0, 60);
        assertSame("Invalid triangle", returnString);
    }

    @Test
    void testEquilateral() {
        String returnString = triangleClass.triangleType(60, 60, 60);
        assertSame("Equilateral triangle", returnString);
    }

    @Test
    void testIsoscelesSide1() {
        String returnString = triangleClass.triangleType(58, 61, 61);
        assertSame("Isosceles triangle", returnString);

        returnString = triangleClass.triangleType(2, 89, 89);
        assertSame("Isosceles triangle", returnString);
    }

    @Test
    void testIsoscelesSide2() {
        String returnString = triangleClass.triangleType(61, 58, 61);
        assertSame("Isosceles triangle", returnString);

        returnString = triangleClass.triangleType(89, 2, 89);
        assertSame("Isosceles triangle", returnString);
    }

    @Test
    void testIsoscelesSide3() {
        String returnString = triangleClass.triangleType(61, 61, 58);
        assertSame("Isosceles triangle", returnString);

        returnString = triangleClass.triangleType(89, 89, 2);
        assertSame("Isosceles triangle", returnString);
    }

    @Test
    void testScalene() {
        String returnString = triangleClass.triangleType(50, 60, 70);
        assertSame("Scalene triangle", returnString);

        returnString = triangleClass.triangleType(90, 40, 50);
        assertSame("Scalene triangle", returnString);
    }
}